//
// Created by "Yaokai Liu" on 12/10/22.
//
#include "catcode.h"

catcode get_catcode(xChar * _the_char) {
    xSize s = _the_char->header.size;
    if (s != 1) {
        return (s == 0) ? 0 : 255;
    } else {
        xuInt e = ord(_the_char);
        return (e <= 127) ? e : 255;
    }
}
