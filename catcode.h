//
// Created by "Yaokai Liu" on 12/6/22.
//

#ifndef XC_CATCODE_H
#define XC_CATCODE_H

#include "xchar.h"

typedef xuByte catcode;

catcode get_catcode(xChar * _the_char);

#endif //XC_CATCODE_H
